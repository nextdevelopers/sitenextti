<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Next Tecnologia da Informação</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<!--Primeira barra de menu na qual ira sumir-->
<header class="fixed-top">
    <nav class="navbar navbar-expand-lg menuFirst" id="menuFirstHide">
        <div class="container">
            <button class="navbar-toggler btnNavBarToggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fas fa-angle-down"></i></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarTogglerDemo01">
                <ul class="navbar-nav">
                    <li class="nav-item" id="linksIcones">
                        <a href="https://www.facebook.com/nexttecnologiadainfo/" ><i class="fab fa-facebook-square"></i></a>
                        <a href="https://twitter.com/next_ti1" ><i class="fab fa-twitter-square"></i></a>
                        <a href="https://www.instagram.com/nexttecnologia/" ><i class="fab fa-instagram"></i></a>
                        <a href="https://www.linkedin.com/company/next---tecnologia-da-informa%C3%A7%C3%A3o/" ><i class="fab fa-linkedin"></i></a>
                    </li>
                </ul>
                <ul class="navbar-nav menuIcon">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-tie"></i>
                            Área do Comercial
                        </a>
                        <div class="dropdown-menu" id="DropdownMenuInicial" aria-labelledby="navbarDropdown">
                            <div class="container">
                                <div class="itemMenu">
                                    <i class="far fa-clock"></i>
                                    2ª a 6ª feira das 8h às 18h
                                </div>
                                <div class="itemMenu">
                                    <i class="far fa-envelope"></i> contato@nexttecnologiadainformacao.com.br
                                </div>
                                <div>
                                    <button type="button" class="btn btn-success btn-lg btn-block">
                                        <i class="fab fa-whatsapp"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-comments"></i>
                                Área Cliente
                        </a>
                        <div class="dropdown-menu" id="DropdownMenuInicial" aria-labelledby="navbarDropdown">
                            <div class="container">
                                <div class="itemMenu">
                                    <i class="far fa-clock"></i>
                                    2ª a 6ª feira das 8h às 18h
                                </div>

                                <div class="itemMenu">
                                    <i class="far fa-envelope"></i> contato@nexttecnologiadainformacao.com.br
                                </div>
                                <div>
                                    <button type="button" class="btn btn-success btn-lg btn-block">
                                        <i class="fab fa-whatsapp"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fas fa-handshake" id="iconTrabalho"></i>
                            Trabalhe Conosco
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<!-- Segundo menu-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light " id="menuSecondHide">
        <div class="container">
            <a href="#" class="navbar-brand imageLogo"><img src="img/logo-oficial.png" class="img-fluid" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo02">
                <ul class="navbar-nav" id="navbarMenu">
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sobre Nós</a>
                    </li>
                    <li class="nav-item dropdown services">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Serviços
                        </a>
                        <div class="dropdown-menu servicesList" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Gestão de Tecnologia da Informação</a>
                            <a class="dropdown-item" href="#">Soluções integradas de aplicativos</a>
                            <a class="dropdown-item" href="#">Desenvolvimento de Sistemas</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Noticias</a>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="btn btn-primary"> Contato</button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
