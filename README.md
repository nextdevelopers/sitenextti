# [Next Tecnologia da Informação](http://nexttecnologiadainformacao.com.br/) 

-----------------------
Projeto de reformulação do site da empresa Next.  
O projeto contará com uma identidade visual para o site, novas funcionalidades e a melhora da experiência para o usuário.
-----------------------

> Equipe do projeto

```
Fabiano Sinhorelli - Gerente
Gabriel Modesto - Desenvolvedor
André Gama - Desenvolvedor
Samuel Quintana - Designer
```

> Linguagens utilizadas

```
HTML5 & CSS3
JavaScript
PHP 7.1
(Bootstrap 4.0)[https://getbootstrap.com/docs/4.0/getting-started/introduction/]
```

> Site de referência

-----------------------

[Efeito do texto](https://technext.github.io/linkweb/portfolio.html)

[Menus](https://www.penso.com.br/)

[Efeito do Texto e Posicionamento](http://demo.themewagon.com/preview/elixir-elegant-html5-bootstrap-template-consultancy-agency-website)

-----------------------


> Mudanças
```
25/02/2019:

```