<?php require('header.php'); ?>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="img/ambiente-de-trabalho.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <div class="col-4 textoCarrosel animated fadeInUp">
                        <h3>TESTE TITULO</h3>
                        <p class="text-lg-left"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ipsum, officiis omnis
                            perferendis recusandae voluptatem.
                        </p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="img/area-de-trabalho-atividade.jpg" alt="Second slide">
              <div class="carousel-caption d-none d-md-block">
                  <div class="col-4 textoCarrosel animated fadeInUp">
                      <h3>TESTE TITULO</h3>
                      <p class="text-lg-left"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ipsum, officiis omnis
                          perferendis recusandae voluptatem.
                      </p>
                  </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="img/area-de-trabalho.jpg" alt="Third slide">
              <div class="carousel-caption d-none d-md-block">
                  <div class="col-4 textoCarrosel animated fadeInUp">
                      <h3>TESTE TITULO</h3>
                      <p class="text-lg-left"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ipsum, officiis omnis
                          perferendis recusandae voluptatem.
                      </p>
                  </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </div>
    <section id="sectionSobreNos">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 divSobreNos"  data-aos="fade-right">
                    <div>
                        <h3>Sobre Nós</h3>
                        <p>A Next Tecnologia, atua no ramo de serviços especializados no mercado B2B, realizando o
                            gerenciamento e suporte em todos os segmentos da TI, utilizando para isso, metodologias
                            ágeis de atendimento e padrão MSP (Managed Service Provider). O foco da Next Tecnologia
                            está no conhecimento profundo sobre o workflow de toda a empresa, não se limitando ao
                            departamento de TI, tornando possível a gestão integrada de todos os setores e
                            potencializando o desempenho, produtividade e lucratividade de nossos clientes.</p>
                    </div>
                </div>
                <div class="col-12 col-lg-6"  data-aos="fade-up">
                    <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="img/2.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/1.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/3.jpg" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/4.jpg" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="sectionServices">
        <h3>Serviços</h3>
        <div class="container">
            <div class="row servicesPadding">
                <div class="col-12 col-lg-6 espacoDireita" id="backgroundColumFirst">
                </div>
                <div class="col-12 col-lg-6" data-aos="fade-up">
                    <h3>Noticias</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur dolores ex
                        explicabo perferendis quia vitae voluptatibus! Consequatur cupiditate maxime quod. Aliquam
                        amet autem consequatur ducimus maiores neque quod recusandae sapiente unde veritatis?
                        Aliquam aliquid animi cumque deserunt error inventore mollitia odit praesentium quaerat
                        quam, quis quo rerum similique sit.
                    </p>
                </div>
            </div>
            <div class="row servicesPadding">
                <div class="col-12 col-lg-6 espacoEsquerda order-lg-2" id="backgroundColumSecond">
                </div>
                <div class="col-12 col-lg-6" data-aos="fade-up">
                    <h3>Noticias</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur dolores ex
                        explicabo perferendis quia vitae voluptatibus! Consequatur cupiditate maxime quod. Aliquam
                        amet autem consequatur ducimus maiores neque quod recusandae sapiente unde veritatis?
                        Aliquam aliquid animi cumque deserunt error inventore mollitia odit praesentium quaerat
                        quam, quis quo rerum similique sit.
                    </p>
                </div>
            </div>
            <div class="row servicesPadding">
                <div class="col-12 col-lg-6 espacoDireita" id="backgroundColumThird">
                </div>
                <div class="col-12 col-lg-6"  data-aos="fade-up">
                    <h3>Noticias</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur dolores ex
                        explicabo perferendis quia vitae voluptatibus! Consequatur cupiditate maxime quod. Aliquam
                        amet autem consequatur ducimus maiores neque quod recusandae sapiente unde veritatis?
                        Aliquam aliquid animi cumque deserunt error inventore mollitia odit praesentium quaerat
                        quam, quis quo rerum similique sit.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="sectionDepoimento">
        <h3 data-aos="fade-up">Depoimentos</h3>
        <h4 data-aos="fade-up">We thanks for all our awesome testimonials! There are hundreds of our happy customers!
            Let's see what others say about Linkweb website template!</h4>
        <div class="container">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="depoimentoFirst">
                            <h4><i class="fas fa-quote-left"></i> Boa Equipe</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A illum ipsa odio optio, quam rem.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="depoimentoFirst">
                            <h4><i class="fas fa-quote-left"></i> Boa Equipe</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A illum ipsa odio optio, quam rem.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="depoimentoFirst">
                            <h4><i class="fas fa-quote-left "></i> Boa Equipe</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A illum ipsa odio optio, quam rem.</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
    <section id="sectionEbook">
        <h4>Como acompanhar e avaliar a rotina de</h4>
        <h3>trabalho de TI em sua empresa</h3>
        <div class="container">
            <div class="row marginEbook">
                <div class="col-12 col-lg-6" data-aos="fade-left">
                    <img src="img/ebook.png" alt="">
                </div>
                <div class="col-12 col-lg-6 textoEbook" data-aos="fade-up">
                    <h4>CAT</h4>
                    <p>Lorem ipsum dolor st amet, consectetur adipisicing elit. Ab amet architecto aut dolores expedita facere hic, illo incidunt labore modi nisi numquam officiis perspiciatis quas repellat repellendus saepe soluta voluptates!</p>
                    <button type="button" class="btn btn-primary btnEbook">Acessar o conteúdo</button>
                </div>
            </div>
        </div>
    </section>
<section id="sectionEmail">
    <div class="container backgroundColor">
        <div class="row">
            <div class="col-12 col-lg-6">
                <h3>Receba nossos novidades por e-mail</h3>
            </div>
            <div class="col-12 col-lg-6">
                <form class="form-inline">
                    <div class="form-group mx-sm-5 mb-2">
                        <label for="inputEmail" class="sr-only">Email</label>
                        <input type="email" class="form-control" id="inputPassword2" placeholder="Enserir seu email">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php require('footer.php'); ?>
